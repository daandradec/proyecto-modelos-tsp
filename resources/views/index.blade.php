<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Duvan Andrade">	
	<title>Proyecto Final Modelos y Simulacion</title>

	<link rel="stylesheet" type="text/css" href="vendor/bootstrap-4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/fonts.css">
	<link rel="stylesheet" type="text/css" href="css/classes.css">
	<link rel="stylesheet" type="text/css" href="css/colors.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
</head>
<body class="min-vh-100">
	
	<header class="container-fluid" style="height:180px;">
		<img src="img/logoU.png" class="float-right" alt="logoU">
	</header>

	<section class="container bg-milk py-2">
		<h1 class="font-roboto-bold mb-5">Proyecto Final - Modelos y Simulación</h1>
		<h3 class="font-size-25px-3c color-dark-gray font-weight-bold">MODELO PARA LA ENTREGA DE PEDIDOS EN LA CIUDAD DE BOGOTÁ</h3>
		<p class="font-quicksand text-justify mb-5 w-80">
			El aumento del precio del combustible ha causado la necesidad  de reducir gastos en el  transporte, así mismo se han generado situaciones donde la eficiencia en la entrega de pedidos se ha visto impactada por la falta de una formalización de los procesos, y por otro lado se pretende maximizar las ventas aumentando la efectividad en la entrega con menos rechazos de los clientes e incrementar la rentabilidad del negocio, con esto nos referimos a aumentar la capacidad de carga, manteniendo las condiciones de calidad de los artículos a transportar, y entregando en el menor tiempo posible.
		</p>
		<h3 class="font-roboto-bold font-size-25px-3c color-cyan-darken-4">Integrantes: </h3>
		<ul class="font-quicksand">
			<li>Duvan Alberto Andrade Cuenca</li>
			<li>Ivonne Daniela Carrasco Caro</li>
			<li>Kevin Alexander Niño Corredor</li>
			<li>Elmar Santofimio Suarez</li>
		</ul>

		<div class="my-3 center">
			<a href={{url('/algorithm')}} class="btn btn-success font-quicksand">Ir al Modelo</a>
		</div>
	</section>
	
	<script type="text/javascript" src="vendor/jquery-3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="vendor/popper-1.14.3/popper.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap-4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
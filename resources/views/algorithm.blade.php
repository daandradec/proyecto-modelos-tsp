<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Duvan Andrade">	
	<title>Proyecto Final Modelos y Simulacion</title>

	<link rel="stylesheet" type="text/css" href="vendor/bootstrap-4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/fonts.css">
	<link rel="stylesheet" type="text/css" href="css/classes.css">
	<link rel="stylesheet" type="text/css" href="css/colors.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<style>
	.nav-item{
	    background-color: white;
	    width:25%;
	    color: black;
	    text-align: center;

	    font-size: 28px;
	    font-family: 'lobster';
	    
	}
	.nav-item:hover{
	    background-color: #dfdfdf;
	}
	</style>
</head>
<body class="min-vh-100">
	
	<header class="container-fluid" style="height:180px;">
		<img src="img/logoU.png" class="float-right" alt="logoU">
	</header>

	<main class="container-fluid">
		<div class="w-80 border mx-auto bg-semi-white shadow">

			<div class="tab-content" id="myTabContent">

				<div class="tab-pane fade pb-2 w-100" id="delete" role="tabpanel" aria-labelledby="delete-tab">
					<h2 class="font-roboto color-dark-gray">Modelo TSP</h2>
					<hr class="mt-0 mb-2"/>
					<div class="w-80 mx-auto">
					<h3 class="font-quicksand font-weight-bold text-muted font-size-20px">VELOCIDAD</h3>
					<form>
						<input type="range" class="custom-range" id="customRange1" min="1" max="3" value="1">
					</form>					
					</div>
					<hr class="mt-1 mb-2"/>

					<div class="container-fluid">
						<div class="row">
							<div class="col-12 col-md-6 mp-0">
								<h3 class="font-roboto font-weight-bold font-size-22px color-dark-gray ml-2">TSP</h3>
								<canvas id="canvasTCP" class="border"></canvas>
								<div class="w-100 p-2">
									<code solution="tcp" class="bg-secondary text-white rounded p-1">Solución:</code>
									<code cost="tcp" class="bg-info text-white rounded p-1">Coste:</code>
								</div>						
							</div>
							<div class="col-12 col-md-6 mp-0">
								<h3 class="font-roboto font-weight-bold font-size-22px color-dark-gray ml-2">Soluciones TSP</h3>
								<canvas id="canvasTCPSOL" class="border"></canvas>
								<div class="w-100 p-2">
									<code solution="tcpsol" class="bg-secondary text-white rounded p-1">Solución:</code>
									<code cost="tcpsol" class="bg-info text-white rounded p-1">Coste:</code>
								</div>						
							</div>
						</div>

						<div class="row">
							<div class="col-12 col-md-6 mp-0">
								<h3 class="font-roboto font-weight-bold font-size-22px color-dark-gray ml-2">DFS</h3>
								<canvas id="canvasDFS" class="border"></canvas>
								<div class="w-100 p-2">
									<code solution="dfs" class="bg-secondary text-white rounded p-1">Solución:</code>
									<code cost="dfs" class="bg-info text-white rounded p-1">Coste:</code>
								</div>						
							</div>
							<div class="col-12 col-md-6 mp-0">
								<h3 class="font-roboto font-weight-bold font-size-22px color-dark-gray ml-2">BFS</h3>
								<canvas id="canvasBFS" class="border"></canvas>
								<div class="w-100 p-2">
									<code solution="bfs" class="bg-secondary text-white rounded p-1">Solución:</code>
									<code cost="bfs" class="bg-info text-white rounded p-1">Coste:</code>
								</div>						
							</div>
						</div>	

						<hr/>
						<div class="row">
							<div class="col-12 col-md-6 mp-0">
								<h3 class="font-roboto font-weight-bold font-size-22px color-dark-gray ml-2">COSTO UNIFORME</h3>
								<canvas id="canvasGREEDY" class="border"></canvas>
								<div class="w-100 p-2">
									<code solution="greedy" class="bg-secondary text-white rounded p-1">Solución:</code>
									<code cost="greedy" class="bg-info text-white rounded p-1">Coste:</code>
								</div>						
							</div>
							<div class="col-12 col-md-6 mp-0">
								<h3 class="font-roboto font-weight-bold font-size-22px color-dark-gray ml-2">A*</h3>
								<canvas id="canvasASTAR" class="border"></canvas>
								<div class="w-100 p-2">
									<code solution="astar" class="bg-secondary text-white rounded p-1">Solución:</code>
									<code cost="astar" class="bg-info text-white rounded p-1">Coste:</code>
								</div>						
							</div>
						</div>	

					</div>
				</div>


				<div class="tab-pane fade show active px-2 pb-2" id="home" role="tabpanel" aria-labelledby="home-tab">
					<h2 class="font-roboto color-dark-gray">Ingreso de Datos</h2>
					<p class="font-quicksand text-muted">Puedes decidir subir los datos manualmente. <span class="font-weight-bold">¡El modelo tomara conexiones de todos con todos usando como coste la distancia radial entre latitud y longitud!</span></p>
					<form class="mt-3 py-2">
						<div inputs="true">	
							<div class="d-inline-block">
								<div class="d-inline-block">
									<label>Latitud:</label>
									<input type="text" class="form-control" style="max-width:380px;" onkeydown="handleInputChange(this,0,true)" onkeyup="handleInputChange(this,0,true)" value="">
								</div>
								<div class="d-inline-block">
									<label class="w-100">Longitud: <span class="float-right mr-2 badge badge-secondary">0</span></label>
									<input type="text" class="form-control" style="max-width:380px;" onkeyup="handleInputChange(this,0,false)" onkeydown="handleInputChange(this,0,false)" value="">
								</div>						
							</div>						
						</div>					
						
						<div class="form-group mt-3 w-100">
							<input type="button" class="btn btn-primary btn-block font-weight-bold font-size-20px" value="+" onclick="appendInputs()">
						</div>
						
						<hr/>

						<div class="form-group center py-1">
							<div>
								<p class="font-quicksand text-muted">O cargarlos a traves de un archivo de excel <span class="font-weight-bold">¡El modelo utilizara el excel unicamente como datos de entrada (no usara los campos de texto)!</span></p>
								<label for="file">
									<span class="btn btn-success">Cargar Archivo Excel</span>
									<input type="file" name="file" id="file">
								</label>
								<p class="font-roboto font-size-14px mp-0" filename="true"></p>
								<p class="font-lobster font-size-18px mt-3 mb-3 py-0 color-petshappy-main-green" fileconfirmation="true"></p>
								<button class="btn btn-outline-info" id="btnpopover" role="button" type="button" data-content="<img src='img/ejemplo_entrada.PNG' alt='ejemplo'/>" data-html="true" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-container="body">Ver Excel de ejemplo</button>
							</div>
						</div>
					</form>

					<div class="d-flex justify-content-end">
						<a class="btn btn-primary btn-sm" id="show-tab" data-toggle="tab" href="#delete" role="tab" aria-controls="delete" aria-selected="false">Siguiente &gt;</a>					
					</div>
                </div>
                
			</div>				
		</div>
	</main>

	<script type="text/javascript" src="vendor/jquery-3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="vendor/popper-1.14.3/popper.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap-4.3.1/js/bootstrap.min.js"></script>
	<script src="js/modelo/validateNumeric.js"></script>
	<script src="js/modelo/coordinatesDataInputs.js"></script>
	<script src="js/modelo/appendInputs.js"></script>
	<script src="js/modelo/loadFile.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/jszip.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.js"></script>

	<script type="text/javascript" src="js/PriorityQueue.js"></script>
	<script type="text/javascript" src="js/particionesEquidistantes.js"></script>
	<script type="text/javascript" src="js/modelo/createEdgesGraph.js"></script>
	<script type="text/javascript" src="js/modelo/mapVertexToString.js"></script>
	<script type="text/javascript" src="js/calculos.js"></script>
	<script type="text/javascript" src="js/Grafo.js"></script>
	<script type="text/javascript" src="js/TCP.js"></script>
	<script type="text/javascript" src="js/DFS.js"></script>
	<script type="text/javascript" src="js/BFS.js"></script>
	<script type="text/javascript" src="js/GREEDY.js"></script>
	<script type="text/javascript" src="js/ASTAR.js"></script>
	<script type="text/javascript" src="js/Grafico.js"></script>
	<script type="text/javascript" src="js/Motor.js"></script>
	<script>
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			if($('#delete').hasClass("active")){			
				if(file === undefined || typeof file === 'undefined'){
					mapVertexToString();
					createEdgesGraph();
					new Motor(vertices, aristas , coordenadas, false);							
				}else
					new Motor(vertices, aristas , coordenadas, true);						
			}
		})	
		$('#btnpopover').popover()
		$('#btnpopover').on('click',function(){
			$('#btnpopover').popover('toggle')
		})		
	</script>
</body>
</html>
class GREEDY{
	constructor(grafo){
		this.estructura_dato = new PriorityQueue();
		this.estado_inicial = grafo.getEstadoInicial();
        this.estructura_dato.enqueue({estado:this.estado_inicial, solucion:[this.estado_inicial], visitados:[this.estado_inicial], priority:0})
		this.costo_total = 0;
	}

	iterar(grafo){
        //console.log(this.toString())
		if(!this.estructura_dato.isEmpty()){
			const jsonNodo = this.estructura_dato.dequeue();
			const estado_actual = jsonNodo.estado;
            const solucion_actual = jsonNodo.solucion;
            const visitados_actual = Array.from(jsonNodo.visitados);
            const coste_actual = jsonNodo.priority;
			//console.log(estado_actual);
			
			
			if(grafo.getEstadoFinal() === estado_actual && solucion_actual.length > 1){ // para que no se detenga si es el estado inicial el objetivo	
				this.estructura_dato.enqueueHead({estado:estado_actual, solucion: solucion_actual, visitados: visitados_actual, priority: coste_actual})	
				this.costo_total = coste_actual;
				return true;
			}
			else{
                const nodosDondePuedoIr = grafo.nodosDondePuedoIr(estado_actual);
                var flagAllVisited = true;
				//console.log(nodosDondePuedoIr)
				for(var i = 0; i < nodosDondePuedoIr.length; ++i){
					const nodo_nuevo = nodosDondePuedoIr[i];

					if(visitados_actual.indexOf(nodo_nuevo) < 0){
                        const visitados_actual_nuevo = Array.from(jsonNodo.visitados);                        
                        visitados_actual_nuevo.push(nodo_nuevo);
                        const coste_nuevo = grafo.costeTotalDelPath(coste_actual, estado_actual, nodo_nuevo);
						this.estructura_dato.enqueue({estado:nodo_nuevo, solucion: solucion_actual.concat([nodo_nuevo]), visitados:visitados_actual_nuevo, priority: coste_nuevo})
                        flagAllVisited = false;
                    }
                }
                if(flagAllVisited && nodosDondePuedoIr.indexOf(this.estado_inicial) > -1){
                    const coste_final = grafo.costeTotalDelPath(coste_actual, estado_actual, this.estado_inicial);
                    this.estructura_dato.enqueue({estado:this.estado_inicial, solucion: solucion_actual.concat([this.estado_inicial]), visitados:visitados_actual, priority: coste_final})
                }
				//console.log("--------")
			}
			return false;
		}
		return true        
	}

	toString(){
		return JSON.stringify(this.estructura_dato)
	}
}
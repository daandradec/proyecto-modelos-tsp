class Motor{
	
	constructor(vertices,aristas,coordenadas, heuristicFlag){
		const coordenads_mapeada = coordenadas.map(function(jsonCoordenada){ return {x:transformadaLatitud(jsonCoordenada.x),y:transformadaLongitud(jsonCoordenada.y)} })
		this.grafo = new Grafo(vertices, aristas, coordenadas);
		this.grafo.setEstadoInicial(vertices[0]);
		this.grafo.setEstadoFinal(vertices[0]);

		this.tcp = new TCP(this.grafo);
		this.dfs = new DFS(this.grafo);
		this.bfs = new BFS(this.grafo);
		this.greedy = new GREEDY(this.grafo);
		this.astart = new ASTAR(this.grafo, heuristicFlag);

		this.grafico_tcp = new Grafico('canvasTCP', coordenads_mapeada, coordenadas, vertices);
		this.grafico_tcp_sol = new Grafico('canvasTCPSOL', coordenads_mapeada, coordenadas, vertices);
		this.grafico_dfs = new Grafico('canvasDFS', coordenads_mapeada, coordenadas, vertices);	
		this.grafico_bfs = new Grafico('canvasBFS', coordenads_mapeada, coordenadas, vertices);	
		this.grafico_greedy = new Grafico('canvasGREEDY', coordenads_mapeada, coordenadas, vertices);	
		this.grafico_astar = new Grafico('canvasASTAR', coordenads_mapeada, coordenadas, vertices);	

		this.flagStopLoopTCP = false;
		this.flagStopRenderTCP = false;
		this.flagStopLoopDFS = false;
		this.flagStopRenderDFS = false;
		this.flagStopLoopBFS = false;
		this.flagStopRenderBFS = false;		
		this.flagStopLoopGREEDY = false;
		this.flagStopRenderGREEDY = false;		
		this.flagStopLoopASTAR = false;
		this.flagStopRenderASTAR = false;				

		var _this = this;
		this.funcion_actualizar = function(){_this.actualizar()};
		this.funcion_renderizar = function(){_this.renderizar()};
		this.time_loop = 200;
		this.update_timeout = setTimeout(this.funcion_actualizar, this.time_loop);
		this.render_timeout = setTimeout(this.funcion_renderizar, this.time_loop/2);

		this.resize_graficos = function(){_this.resizeGraficos();}
		window.addEventListener('resize', this.resize_graficos, false);
		$("#customRange1").on("change input", function(e){
			window.clearTimeout(_this.update_timeout);
			window.clearTimeout(_this.render_timeout);
			window.setTimeout(function(){
				switch(e.target.value){
					case "1":
						_this.time_loop = 200;
						_this.update_timeout = setTimeout(_this.funcion_actualizar, _this.time_loop);
						_this.render_timeout = setTimeout(_this.funcion_renderizar, _this.time_loop/2);					
						break;
					case "2":
						_this.time_loop = 20;
						_this.update_timeout = setTimeout(_this.funcion_actualizar, _this.time_loop);
						_this.render_timeout = setTimeout(_this.funcion_renderizar, _this.time_loop/2);					
						break;
					case "3":
						_this.time_loop = 2;
						_this.update_timeout = setTimeout(_this.funcion_actualizar, _this.time_loop);
						_this.render_timeout = setTimeout(_this.funcion_renderizar, _this.time_loop/2);											
						break;
				}
			},5);
			
		})
	}

	actualizar() {
		var flagIterate = false;
		flagIterate = this.actualizarTCP() || flagIterate;	
		flagIterate = this.actualizarDFS() || flagIterate;	
		flagIterate = this.actualizarBFS() || flagIterate;	
		flagIterate = this.actualizarGREEDY() || flagIterate;		
		flagIterate = this.actualizarASTAR() || flagIterate;
		if(flagIterate)
			this.update_timeout = setTimeout(this.funcion_actualizar, this.time_loop);
	}

	renderizar(){
		var flagIterate = false;
		flagIterate = this.renderizarTCP() || flagIterate;
		flagIterate = this.renderizarDFS() || flagIterate;
		flagIterate = this.renderizarBFS() || flagIterate;	
		flagIterate = this.renderizarGREEDY() || flagIterate;
		flagIterate = this.renderizarASTAR() || flagIterate;
		if(flagIterate)	
			this.render_timeout = setTimeout(this.funcion_renderizar, this.time_loop)
	}

	/* CODIGO DE UPDATE Y RENDER TCP */

	actualizarTCP(){
		if(!this.flagStopLoopTCP){
			if(this.tcp.iterar(this.grafo))
				this.flagStopLoopTCP = true;
		}
		return !this.flagStopLoopTCP;
	}

	renderizarTCP(){
		if(!this.flagStopRenderTCP){
			var solucion = undefined;
			if(this.tcp.estructura_dato.length)
				solucion = this.tcp.estructura_dato[0].solucion;	
			else
				solucion = this.tcp.ultimo_registro_leido.solucion;
			const conecciones = this.grafo.obtenerConecciones(solucion);
			this.grafico_tcp.render(conecciones);
			if(this.tcp.solucionEncontrada()){
				const solucion_tcp_encontrada = this.tcp.solucion_actual_encontrada.solucion;
				const solucion_tcp_optima = this.tcp.mejor_solucion.solucion;
				const conneciones_optima = this.grafo.obtenerConecciones(solucion_tcp_optima);
				this.grafico_tcp_sol.render(conneciones_optima);
				document.querySelector("code[solution='tcp']").innerText = "Solución: "+solucion_tcp_encontrada.toString().replace(/,/g, '->');
				document.querySelector("code[cost='tcp']").innerText = "Coste: "+ this.tcp.costo_total_sol_actual;
				document.querySelector("code[solution='tcpsol']").innerText = "Solución: "+solucion_tcp_optima.toString().replace(/,/g, '->');
				document.querySelector("code[cost='tcpsol']").innerText = "Coste: "+ this.tcp.costo_total;				
				this.tcp.offSolucionEncontrada();
			}
			if(this.flagStopLoopTCP){
				this.flagStopRenderTCP = true;
				console.log("Información adicional TCP");
				console.log("peor solucion:")
				console.log(this.tcp.peor_solucion);
				console.log(this.tcp.calcularCostoPath(this.grafo, this.tcp.peor_solucion.solucion));
				console.log("mejor solucion:")
				console.log(this.tcp.mejor_solucion);
				console.log(this.tcp.costo_total);
				console.log("costo promedio:");
				console.log( (this.tcp.costo_total + this.tcp.calcularCostoPath(this.grafo, this.tcp.peor_solucion.solucion))/2 );
			}
		}
		return !this.flagStopRenderTCP;
	}

	/* CODIGO DE UPDATE Y RENDER DFS */
	actualizarDFS(){
		if(!this.flagStopLoopDFS){
			if(this.dfs.iterar(this.grafo))
				this.flagStopLoopDFS = true;
		}
		return !this.flagStopLoopDFS;
	}

	renderizarDFS(){
		if(!this.flagStopRenderDFS){
			const solucion = this.dfs.estructura_dato[this.dfs.estructura_dato.length-1].solucion;
			const conecciones = this.grafo.obtenerConecciones(solucion);
			this.grafico_dfs.render(conecciones)
			if(this.flagStopLoopDFS){
				document.querySelector("code[solution='dfs']").innerText = "Solución: "+solucion.toString().replace(/,/g, '->');
				document.querySelector("code[cost='dfs']").innerText = "Coste: "+ this.dfs.costo_total;
				this.flagStopRenderDFS = true;
			}
		}
		return !this.flagStopRenderDFS;
	}




	/* CODIGO DE UPDATE Y RENDER BFS */
	actualizarBFS(){
		if(!this.flagStopLoopBFS){
			if(this.bfs.iterar(this.grafo))
				this.flagStopLoopBFS = true;
		}
		return !this.flagStopLoopBFS;		
	}

	renderizarBFS(){
		if(!this.flagStopRenderBFS){
			const solucion = this.bfs.estructura_dato[0].solucion;
			const conecciones = this.grafo.obtenerConecciones(solucion);
			this.grafico_bfs.render(conecciones)
			if(this.flagStopLoopBFS){
				document.querySelector("code[solution='bfs']").innerText = "Solución: "+solucion.toString().replace(/,/g, '->');
				document.querySelector("code[cost='bfs']").innerText = "Coste: "+ this.bfs.costo_total;
				this.flagStopRenderBFS = true;
			}
		}
		return !this.flagStopRenderBFS;
	}




	/* CODIGO DE UPDATE Y RENDER GREEDY */
	actualizarGREEDY(){
		if(!this.flagStopLoopGREEDY){
			if(this.greedy.iterar(this.grafo))
				this.flagStopLoopGREEDY = true;
		}
		return !this.flagStopLoopGREEDY;		
	}

	renderizarGREEDY(){
		if(!this.flagStopRenderGREEDY){
			const solucion = this.greedy.estructura_dato.front().solucion;
			const conecciones = this.grafo.obtenerConecciones(solucion);
			this.grafico_greedy.render(conecciones)
			if(this.flagStopLoopGREEDY){
				document.querySelector("code[solution='greedy']").innerText = "Solución: "+solucion.toString().replace(/,/g, '->');
				document.querySelector("code[cost='greedy']").innerText = "Coste: "+ this.greedy.costo_total;
				this.flagStopRenderGREEDY = true;
			}
		}
		return !this.flagStopRenderGREEDY;
	}



	/* CODIGO DE UPDATE Y RENDER A*  */
	actualizarASTAR(){
		if(!this.flagStopLoopASTAR){
			if(this.astart.iterar(this.grafo))
				this.flagStopLoopASTAR = true;
		}
		return !this.flagStopLoopASTAR;		
	}

	renderizarASTAR(){
		if(!this.flagStopRenderASTAR){
			const solucion = this.astart.estructura_dato.front().solucion;
			const conecciones = this.grafo.obtenerConecciones(solucion);
			this.grafico_astar.render(conecciones)
			if(this.flagStopLoopASTAR){
				document.querySelector("code[solution='astar']").innerText = "Solución: "+solucion.toString().replace(/,/g, '->');
				document.querySelector("code[cost='astar']").innerText = "Coste: "+ this.astart.costo_total;
				this.flagStopRenderASTAR = true;
			}
		}
		return !this.flagStopRenderASTAR;
	}



	/* RESIZE */
	resizeGraficos(){
		this.grafico_tcp.resize();
		this.grafico_tcp_sol.resize();
		this.grafico_dfs.resize();
		this.grafico_bfs.resize();
		this.grafico_greedy.resize();
		this.grafico_astar.resize();
	}
}
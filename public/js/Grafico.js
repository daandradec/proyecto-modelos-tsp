class Grafico{

	constructor(idCanvas, data, dataOriginal, vertex){
		this.canvas = document.getElementById(idCanvas);
		this.canvas.setAttribute('width', this.canvas.parentElement.clientWidth);
		this.canvas.setAttribute('height', 500);
		this.initialWidth = this.canvas.clientWidth;
		this.width = this.canvas.clientWidth;
		this.height = this.canvas.clientHeight;	
		this.ctx = this.canvas.getContext('2d');
		this.data = data;
		this.dataOriginal = dataOriginal;
		this.vertex = vertex;
		this.connections = undefined;

		this.h_factor_division = 20;
		this.width_square = Math.floor(this.width/this.h_factor_division);
		this.height_square = Math.floor(this.height/this.h_factor_division);
		this.maxLimits = findMaxXYInArrayOfJson(data);
		this.scale_factor_x = this.width / (this.maxLimits.xMax - this.maxLimits.xMin);
		this.scale_factor_y = this.height / (this.maxLimits.yMax - this.maxLimits.yMin);

		const maxLimitsOriginal = findMaxXYInArrayOfJson(dataOriginal);
		this.axisXNumbers = particionesEquidistantes(maxLimitsOriginal.xMin, maxLimitsOriginal.xMax, this.h_factor_division);		
		this.axisYNumbers = particionesEquidistantes(maxLimitsOriginal.yMin, maxLimitsOriginal.yMax, this.h_factor_division);		

		this.generateGrid();
		this.drawPoints();
	}

	generateGrid(){		
		/* CONFIGURATION */
		this.ctx.strokeStyle  = 'gray';
		this.ctx.lineWidth = 1.0;
		this.ctx.font = 'bold 18px serif';
		
		/* HORIZONTALES */
		for(var i = 1; i < this.height_square-1; ++i)
			this.drawLine(this.h_factor_division*2,i*this.h_factor_division,this.width,i*this.h_factor_division);
		/* VERTICALES */
		for(var i = 2; i <= this.width_square; ++i)
			this.drawLine(i*this.h_factor_division,0,i*this.h_factor_division,this.height-this.h_factor_division*2);
		
		this.ctx.save();
		this.ctx.font = '10px serif';
		
		/* NUMEROS DEL EJE X */
		for(var i = 0; i < this.axisXNumbers.length; ++i){
			this.ctx.save();
			this.ctx.translate((i+1)*this.width_square, (this.height_square-2)*this.h_factor_division + 3);
			this.ctx.rotate(gradosARadianes(90));
			this.drawText(this.axisXNumbers[i], 0, 0,'black');
			this.ctx.restore();
		}

		/* NUMEROS DEL EJE Y */
		for(var i = 1; i < this.axisYNumbers.length - 1; ++i){ //var i = this.axisYNumbers.length - 2; i >= 1; --i
			this.drawText(this.axisYNumbers[i], this.h_factor_division - 3, i*this.height_square, 'black');
		}		
		
		/* LABEL EJE Y */
		this.ctx.restore();
		this.ctx.save();

		const textLongitud = "Longitud";
		this.ctx.translate(5, this.height/2 - this.ctx.measureText(textLongitud).width/2,'black');
		this.ctx.rotate(gradosARadianes(90));
		this.drawText(textLongitud, 0, 0,'black');


		/* LABEL EJE X */
		this.ctx.restore();
		this.ctx.save();

		const textLatitud = "Latitud";
		this.drawText(textLatitud, this.width/2 - this.ctx.measureText(textLatitud).width/2, (this.height_square)*this.h_factor_division - 3 ,'black');
	}

	drawLine(x, y, x2, y2){
		this.ctx.beginPath();
		this.ctx.moveTo(x,y)
		this.ctx.lineTo(x2, y2);
		this.ctx.stroke();
		this.ctx.closePath();	
	}

	drawPoints(){		
		for(var i = 0;i < this.data.length; ++i){
			const x = this.transformCoordinateX(this.data[i].x);
			const y = this.revertCanvasYtoCartesianAndTransform(this.data[i].y);
			if(i === 0){
				this.ctx.save();
				this.ctx.shadowColor = '#56ff00';
				this.ctx.shadowBlur = 5;
				this.drawCircle(x,y,this.h_factor_division/2);
				this.ctx.restore();
			}else
				this.drawCircle(x,y,this.h_factor_division/2);			
			if(y <= this.height/2)
				this.drawText(this.vertex[i], x - this.h_factor_division/2, y+(this.h_factor_division*1.35),'green');
			else
				this.drawText(this.vertex[i], x - this.h_factor_division/2, y-(this.h_factor_division*1.35),'green');
		}
	}

	transformCoordinateX(x){
		return Math.min(Math.max(this.h_factor_division*2, (x-this.maxLimits.xMin)*this.scale_factor_x), this.width-this.h_factor_division*2);
	}

	revertCanvasYtoCartesianAndTransform(y){
		return Math.abs(this.height - this.transformCoordinateY(y));
	}
	transformCoordinateY(y){
		return Math.min(Math.max(this.h_factor_division*2, (y-this.maxLimits.yMin)*this.scale_factor_y), this.height-this.h_factor_division);	
	}

	drawCircle(x,y,r){
		this.ctx.fillStyle  = '#a90b0b';
		this.ctx.beginPath();
		this.ctx.arc(x,y,r,0,2*Math.PI,false);
		this.ctx.fill();
		this.ctx.closePath();

		this.ctx.fillStyle  = '#ce1b1b';
		this.ctx.beginPath();
		this.ctx.arc(x,y,r-2,0,2*Math.PI,false);
		this.ctx.fill();
		this.ctx.closePath();	

		this.ctx.fillStyle  = 'white';
		this.ctx.beginPath();
		this.ctx.arc(x-3,y-3,r-8,0,2*Math.PI,false);
		this.ctx.fill();
		this.ctx.closePath();				
	}

	drawText(text, x , y, fillColor){
		this.ctx.fillStyle  = fillColor;
		this.ctx.fillText(text, x, y);
	}

	clearScreen(){
		this.ctx.clearRect(0, 0, this.width, this.height)
	}


	render(connections){
		this.clearScreen();
		this.ctx.lineWidth = 2.0;
		for(var i = 0; i < this.data.length;++i){
			for(var j = 0;j < connections[i].length; ++j)
				this.drawLine(this.transformCoordinateX(this.data[i].x),this.revertCanvasYtoCartesianAndTransform(this.data[i].y),this.transformCoordinateX(this.data[connections[i][j]].x),this.revertCanvasYtoCartesianAndTransform(this.data[connections[i][j]].y));
		}

		this.connections = connections;
		this.ctx.restore();
		this.generateGrid();
		this.drawPoints();
	}


	resize(){
		this.canvas.setAttribute('width', this.canvas.parentElement.clientWidth);
		this.canvas.setAttribute('height', this.canvas.parentElement.clientWidth*500/this.initialWidth);
		this.width = this.canvas.clientWidth;
		this.height = this.canvas.clientHeight;
		this.width_square = Math.floor(this.width/this.h_factor_division);
		this.height_square = Math.floor(this.height/this.h_factor_division);
		this.scale_factor_x = this.width / (this.maxLimits.xMax - this.maxLimits.xMin);
		this.scale_factor_y = this.height / (this.maxLimits.yMax - this.maxLimits.yMin);		
		if(this.connections !== undefined && typeof this.connections !== 'undefined' && this.connections !== null)
			this.render(this.connections);
		else{
			this.clearScreen();
			this.generateGrid();
			this.drawPoints();
		}
	}
}
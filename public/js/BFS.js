class BFS{
	constructor(grafo){
		this.estructura_dato = []
		this.estado_inicial = grafo.getEstadoInicial();
		this.estructura_dato.push({estado:this.estado_inicial, solucion:[this.estado_inicial], visitados:[this.estado_inicial]});
		this.costo_total = 0;
	}

	iterar(grafo){
        //console.log(this.toString())
		if(this.estructura_dato.length){
			const jsonNodo = this.estructura_dato.shift();
			const estado_actual = jsonNodo.estado;
            const solucion_actual = jsonNodo.solucion;
            const visitados_actual = Array.from(jsonNodo.visitados);
			//console.log(estado_actual);
			
			
			if(grafo.getEstadoFinal() === estado_actual && solucion_actual.length > 1){ // para que no se detenga si es el estado inicial el objetivo	
				this.estructura_dato.unshift({estado:estado_actual, solucion: solucion_actual, visitados: visitados_actual})
				this.costo_total = this.calcularCostoPath(grafo, solucion_actual);
				return true;
			}
			else{
                const nodosDondePuedoIr = grafo.nodosDondePuedoIr(estado_actual);
                var flagAllVisited = true;
				//console.log(nodosDondePuedoIr)
				for(var i = 0; i < nodosDondePuedoIr.length; ++i){
					const nodo_nuevo = nodosDondePuedoIr[i];

					if(visitados_actual.indexOf(nodo_nuevo) < 0){
						const visitados_actual_nuevo = Array.from(jsonNodo.visitados);
						visitados_actual_nuevo.push(nodo_nuevo);
						this.estructura_dato.push({estado:nodo_nuevo, solucion: solucion_actual.concat([nodo_nuevo]), visitados:visitados_actual_nuevo})
                        flagAllVisited = false;
                    }
                }
                if(flagAllVisited){
                    this.estructura_dato.push({estado:this.estado_inicial, solucion: solucion_actual.concat([this.estado_inicial]), visitados:visitados_actual})
                }
				//console.log("--------")
			}
			return false;
		}
		return true
	}

	calcularCostoPath(grafo, solucion){
        var costo = 0;
        for(var i = 0; i < solucion.length - 1;++i){
            costo = grafo.costeTotalDelPath(costo, solucion[i], solucion[i+1])
        }
        return costo;
	}
	
	toString(){
		return JSON.stringify(this.estructura_dato)
	}
}
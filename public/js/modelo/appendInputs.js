var count = 1;

function appendInputs(){
    const inputs = document.querySelector("div[inputs='true']");
    inputs.innerHTML += `
        <div class="d-inline-block">
            <div class="d-inline-block">
                <label>Latitud:</label>
                <input type="text" class="form-control" style="max-width:380px;" ${`onkeydown='handleInputChange(this,${count},${true})' onkeyup='handleInputChange(this,${count},${true})'`}>
            </div>
            <div class="d-inline-block">
                <label class="w-100">Longitud: <span class="float-right mr-2 badge badge-secondary">${count}</span></label>
                <input type="text" class="form-control" style="max-width:380px;" ${`onkeydown='handleInputChange(this,${count},${false})' onkeyup='handleInputChange(this,${count},${false})'`}">
            </div>						
        </div>
    `
    coordenadas.push({x:0,y:0});
    vertices.push(count);
    ++count;

    for(var i = 0;i < inputs.children.length-1;++i){
        const child_1 = inputs.children[i];
        const input_1 = child_1.children[0].children[1];
        const input_2 = child_1.children[1].children[1];
        input_1.value = coordenadas[i].x;
        input_2.value = coordenadas[i].y;
    }
}
function valideNumeric(e){
    // /([0-9]+[.|,][0-9])|([0-9][.|,][0-9]+)|([0-9]+)/g​
    if(e.value.charAt(e.value.length-1) === '.'){
        if( (e.value.match(/\./g) || []).length > 1 )
            e.value = e.value.substring(0,e.value.length-1)
    }else
        e.value= e.value.replace(/[^-\d.]/,'');
}
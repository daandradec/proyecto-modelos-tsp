function createEdgesGraph(){
    for(var i = 0;i < vertices.length-1;++i){
        for(var j = i+1;j < vertices.length; ++j){
            aristas.push({
                origen:vertices[i],destino:vertices[j],costo:distanciaEntreLatYLong(coordenadas[i].x,coordenadas[i].y,coordenadas[j].x,coordenadas[j].y)
            })
        }
    }
}

function createEdgesWithData(matrixData){
    for(var i = 0;i < vertices.length-1;++i){
        for(var j = i+1;j < vertices.length; ++j){
            aristas.push({ // j - (i+1)
                origen:vertices[i],destino:vertices[j],costo:matrixData[i][j - (i+1)]
            })
        }
    }
}
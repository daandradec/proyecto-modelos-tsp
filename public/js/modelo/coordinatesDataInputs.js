var coordenadas = [{x:0,y:0}];
var vertices = [0];
var aristas = [];


function handleInputChange(e, index, flagLatLon){
    valideNumeric(e);
    if(e.value !== ""){
        if(flagLatLon)
            coordenadas[index] = {x:Number(e.value), y:coordenadas[index].y}
        else
            coordenadas[index] = {x:coordenadas[index].x, y:Number(e.value)}
    }    
}
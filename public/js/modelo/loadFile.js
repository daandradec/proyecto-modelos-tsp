var file = undefined;
$("input[type=file]").on("change",function(e){
    file = e.target.files[0];

    const allowedExtensions = /(.xls|.xlsx|.xlsm)$/i;
    if(allowedExtensions.exec(file.name)){
        $("p[filename='true']").text(file.name);
        $("p[fileconfirmation='true']").text("Su archivo se ha cargado correctamente, haga click en continuar");
        loadDataFromExcel();
    }else{        
        $(e.target).val(null);        
        alert("por favor ingrese un excel, o un excel valido");
    }
    
})

function loadDataFromExcel(){
    var xl2json = new ExcelToJSON();
    xl2json.parseExcel(file);
}

var ExcelToJSON = function() {

    this.parseExcel = function(file) {
      var reader = new FileReader();

      reader.onload = function(e) {
        var data = e.target.result;
        var workbook = XLSX.read(data, {
          type: 'binary'
        });
        workbook.SheetNames.forEach(function(sheetName) {
          // Here is your object
          var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
          var json_object = JSON.stringify(XL_row_object);          
          constructOutputData(JSON.parse(json_object));
        })
      };

      reader.onerror = function(ex) {
        $("p[fileconfirmation='true']").text("");
        $("p[filename='true']").text("");
        file = undefined
        console.log(ex);
        alert(ex);
        coordenadas = [{x:0, y:0}]
        vertices = [0];
        aristas = [];
        count = 1;        
      };

      reader.readAsBinaryString(file);
    };
};

function constructOutputData(jsonExcel){
    coordenadas = []
    vertices = [];
    aristas = [];
    count = 1;
    matrixData = [];    
    for(var i = 0;i < jsonExcel.length; ++i){
        const json = jsonExcel[i];
        const jsonKeys = Object.keys(jsonExcel[i]);
        const latitud = Number(json[jsonKeys[0]]);
        const longitud = Number(json[jsonKeys[1]]);
        coordenadas.push({x:latitud, y:longitud});
        vertices.push(i);
    }

    
    const jsonKeys = Object.keys(jsonExcel[0]);
    for(var j = 2; j < jsonKeys.length; ++j){
        const costesArray = [];
        for(var k = 0; k < jsonExcel.length; ++k){
            const time = jsonExcel[k][jsonKeys[j]];
            if(time !== "0" && time !== 0)
                costesArray.push(Number(time));
        }
        if(costesArray.length)
            matrixData.push(costesArray);
    }    
    mapVertexToString();
    createEdgesWithData(matrixData);
}
function findMaxXYInArrayOfJson(data){	
	var xMax = 0;
	var yMax = 0;
	var xMin = data[0].x;
	var yMin = data[0].y;

	for(var i = 0; i < data.length; ++i){
		const point = data[i];
		if(point.x > xMax)
			xMax = point.x;
		if(point.y > yMax)
			yMax = point.y;
		if(point.x < xMin)
			xMin = point.x;
		if(point.y < yMin)
			yMin = point.y;
	}
	return {xMax: xMax, yMax: yMax, xMin: xMin, yMin: yMin};
}

/* TRIGONOMETRIA ESFERICA */

function gradosARadianes(grados){
	return grados * Math.PI/180;
}

/* DISTANCIA EN METROS */
function distanciaEntreLatYLong(latitud1, longitud1, latitud2, longitud2){
	return (Math.acos( Math.sin(gradosARadianes(latitud1))*Math.sin(gradosARadianes(latitud2)) + Math.cos(gradosARadianes(latitud1)) * Math.cos(gradosARadianes(latitud2)) * Math.cos(gradosARadianes(longitud1) - gradosARadianes(longitud2)) )) * 6372797.560856
}
// fuentes : http://www.mapanet.eu/Resources/Script-Distance.htm, https://www.todopic.com.ar/foros/index.php?topic=26373.0
// https://kerchak.com/cual-es-el-diametro-de-la-tierra/ ,  https://www.coordenadas-gps.com/sistema-de-coordenadas
/* DIAMETRO DE LA TIERRA : 12700 KM -> x: [-90,0] y: [90,12700*10^3] */

/* DOMINIO LATITUD : -90º A 90º */
function transformadaLatitud(latitud){
	return (12700000/(90-(-90)))*(latitud - (-90))
}

/* DOMINIO LONGITUD: -180º A 180º */
function transformadaLongitud(longitud){
	return (12700000/(180-(-180)))*(longitud - (-180))
}
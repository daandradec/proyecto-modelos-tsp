class Grafo{
	constructor(vertices, aristas, coordenadas){
		this.grafo = {}
		this.indicesGrafo = {}
		this.conecciones = {}
		for(var i = 0; i < vertices.length; ++i){
			this.grafo[vertices[i]] = {}
			this.indicesGrafo[vertices[i]] = i;
			this.conecciones[i] = []
		}
		
		this.heuristic_value_min_cost = aristas[0].costo;
		for(var i = 0; i < aristas.length; ++i){
			const v0 = aristas[i].origen;
			const v1 = aristas[i].destino;
			const peso = aristas[i].costo;
			
			this.grafo[v0][v0] = 0;
			this.grafo[v0][v1] =  peso;
			this.grafo[v1][v0] = peso; 

			if(peso < this.heuristic_value_min_cost)
				this.heuristic_value_min_cost = peso;
		}
		this.coordenadas = coordenadas;
		this.vertices = vertices;
	}

	nodosDondePuedoIr(estado){		
		return Object.keys(this.grafo[estado])
	}

	costeTotalDelPath(coste_base, estado_actual, estado_destino){
		return coste_base + this.grafo[estado_actual][estado_destino];
	}

	costeHeuristicaDistancia(estado_actual, estado_destino){
		return distanciaEntreLatYLong(this.coordenadas[this.indicesGrafo[estado_actual]].x,this.coordenadas[this.indicesGrafo[estado_actual]].y,
				this.coordenadas[this.indicesGrafo[estado_destino]].x,this.coordenadas[this.indicesGrafo[estado_destino]].y)
	}

	obtenerConecciones(solucion){
		const nuevasConneciones = JSON.parse(JSON.stringify(this.conecciones));
		for(var i = 0;i < solucion.length - 1; ++i)
			nuevasConneciones[this.indicesGrafo[solucion[i]]].push(this.indicesGrafo[solucion[i+1]])
		return nuevasConneciones
	}


	getEstadoInicial(){
		return this.estadoInicial;
	}

	getEstadoFinal(){
		return this.estadoFinal;
	}

	setEstadoInicial(estado){
		this.estadoInicial = estado;
	}

	setEstadoFinal(estado){
		this.estadoFinal = estado;
	}
}
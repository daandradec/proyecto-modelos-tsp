class TCP{
	constructor(grafo){
		this.estructura_dato = []
		this.estado_inicial = grafo.getEstadoInicial();
		this.estructura_dato.push({estado:this.estado_inicial, solucion:[this.estado_inicial], visitados:[this.estado_inicial]});
        this.mejor_solucion = this.estructura_dato[0];
        this.peor_solucion = this.mejor_solucion;
        this.solucion_actual_encontrada = this.mejor_solucion;
        this.flagSolucionEncontrada = false;

        this.ultimo_registro_leido = this.estructura_dato[0];
        this.flag_primera_solucion = true;
        this.costo_total = 0;
        this.costo_total_sol_actual = 0;
    }

	iterar(grafo){
        //console.log(this.toString())
		if(this.estructura_dato.length){
			const jsonNodo = this.estructura_dato.shift();
			const estado_actual = jsonNodo.estado;
            const solucion_actual = jsonNodo.solucion;
            const visitados_actual = Array.from(jsonNodo.visitados);
            this.ultimo_registro_leido = jsonNodo;
			//console.log(estado_actual);
			
			
			if(grafo.getEstadoFinal() === estado_actual && solucion_actual.length > 1){ // para que no se detenga si es el estado inicial el objetivo	
                // si minimza el costo
                // calcular el costo del solucion n0 -> n1 -> ... -> nN
                if(this.flag_primera_solucion){
                    this.mejor_solucion = {estado:estado_actual, solucion: solucion_actual, visitados: visitados_actual};
                    this.peor_solucion = this.mejor_solucion;
                    this.flag_primera_solucion = false;                    
                    this.costo_total = this.calcularCostoPath(grafo, solucion_actual);
                }else{
                    const costoMejorActual = this.calcularCostoPath(grafo, this.mejor_solucion.solucion);
                    const costoNuevo = this.calcularCostoPath(grafo, solucion_actual);
                    const costoPeorActual = this.calcularCostoPath(grafo, this.peor_solucion.solucion);
                    if(costoNuevo < costoMejorActual){
                        this.mejor_solucion = {estado:estado_actual, solucion: solucion_actual, visitados: visitados_actual};
                        this.costo_total = costoNuevo;                        
                    }       
                    if(costoNuevo > costoPeorActual){
                        this.peor_solucion = {estado:estado_actual, solucion: solucion_actual, visitados: visitados_actual};
                    }                 
                }    
                this.flagSolucionEncontrada = true;
                this.solucion_actual_encontrada = {estado:estado_actual, solucion: solucion_actual, visitados: visitados_actual};
                this.costo_total_sol_actual = this.calcularCostoPath(grafo, solucion_actual);
			}
			else{
                const nodosDondePuedoIr = grafo.nodosDondePuedoIr(estado_actual);
                var flagAllVisited = true;
				//console.log(nodosDondePuedoIr)
				for(var i = 0; i < nodosDondePuedoIr.length; ++i){
					const nodo_nuevo = nodosDondePuedoIr[i];

					if(visitados_actual.indexOf(nodo_nuevo) < 0){
						const visitados_actual_nuevo = Array.from(jsonNodo.visitados);
						visitados_actual_nuevo.push(nodo_nuevo);
						this.estructura_dato.push({estado:nodo_nuevo, solucion: solucion_actual.concat([nodo_nuevo]), visitados:visitados_actual_nuevo})
                        flagAllVisited = false;
                    }
                }
                if(flagAllVisited){
                    this.estructura_dato.push({estado:this.estado_inicial, solucion: solucion_actual.concat([this.estado_inicial]), visitados:visitados_actual})
                }
				//console.log("--------")
			}
			return false;
        }       
		return true
	}

    calcularCostoPath(grafo, solucion){
        var costo = 0;
        for(var i = 0; i < solucion.length - 1;++i){
            costo = grafo.costeTotalDelPath(costo, solucion[i], solucion[i+1])
        }
        return costo;
    }

    offSolucionEncontrada(){
        this.flagSolucionEncontrada = false;
    }

    solucionEncontrada(){
        return this.flagSolucionEncontrada;
    }

	toString(){
		return JSON.stringify(this.estructura_dato)
	}
}
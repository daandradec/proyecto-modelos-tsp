function particionesEquidistantes(a, b, subdivisiones){
	h = (b-a)/subdivisiones;
	lista = []
	for(i = 0; i <= subdivisiones; ++i){
		lista.push( (a + i*h).toFixed(2).substring(0,5) );
	}
	return lista;
}
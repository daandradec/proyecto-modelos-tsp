class PriorityQueue{
    constructor(){
        this.queue = []
    }

    enqueue(item){
        var flagWorstPriority = true;

        for(var i = 0; i < this.queue.length; ++i){
            if(this.queue[i].priority > item.priority){
                this.queue.splice(i, 0, item)
                flagWorstPriority = false;
                break;
            }
        }

        if(flagWorstPriority)
            this.queue.push(item);
    }

    dequeue(){
        if(!this.isEmpty())
            return this.queue.shift();
        return undefined;
    }

    enqueueHead(item){
        this.queue.unshift(item);
    }

    front(){
        return this.queue[0];
    }

    isEmpty(){
        return this.queue.length === 0;
    }

    toString(){
        return JSON.stringify(this.queue);
    }
}
	var canvas = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(canvas, {
    	type: 'line',
    	data:{
    		labels:particionesEquidistantes(4,5,20),
    		datasets:[
    			{
    				data:[5,4,3,2,1,5,3],
    				fill:false
    			},
    			{
    				data:[9,2,3,4,6,8,2,3,4,1,2],
    				fill:false
    			}
    		]
    	},
    	options: {
    		responsive: true,
	        animation: {duration: 0},
	        hover: {animationDuration: 0},
	        responsiveAnimationDuration: 0
    	}
    });

    setTimeout(function(){
    	myChart.data = {
    		labels:particionesEquidistantes(4,5,20),
    		datasets:[
    			{
    				data:[1,2,3,4,6,2,1],
    				fill:false
    			},
    			{
    				data:[4,5,3,4,6,2,1,5,4,1,6],
    				fill:false
    			}
    		]
    	}
    	myChart.update();
    },2000)